# Materiav2 - DEPRECATED: Now hosted at https://gitlab.com/Gsbhasin84/materiav2

A futuristic Material theme for Linux (GTK based desktops only) with rounded edges. Original theme: https://github.com/nana-4/materia-theme

Donations welcome! https://www.paypal.me/materiav2?locale.x=en_US Feel free to ask questions in Issues, and make pull requests. I am aware of a LibreOffice bug where the font and font size text overlap sometimes, but that is an upstream Materia bug and I have no idea how to fix it ;)

Recommended font: Product Sans (I was using Noto Sans when I took these screenshots, but I discovered Product Sans later - it's awesome!) https://befonts.com/product-sans-font.html

Recommended icon theme: Papirus

## Screenshots

Dark-purple
![](https://cdn.discordapp.com/attachments/616061983950635041/630954303456280577/unknown.png)
Dark-orange
![](https://cdn.discordapp.com/attachments/630085034748936202/632375399099400202/unknown.png)
Dark-mate
![](https://cdn.discordapp.com/attachments/630085034748936202/632375486726668288/unknown.png)
Dark-red
![](https://cdn.discordapp.com/attachments/630085034748936202/632375644918906881/unknown.png)
Dark-aqua
![](https://cdn.discordapp.com/attachments/630085034748936202/632408311823597569/unknown.png)
Dark-maia
![](https://i.imgur.com/sadfRJC.png)


Light-aqua
![](https://i.imgur.com/AOsv3kD.png)
Light-mate
![](https://i.imgur.com/hR0WZXy.png)
Light-orange
![](https://i.imgur.com/Kmzs9wn.png)
Light-purple
![](https://i.imgur.com/mNvblZy.png)
Light-red
![](https://i.imgur.com/TzxjDKg.png)
Light-maia
![](https://i.imgur.com/yCG9lj6.png)

Greybird colorscheme:
![](https://cdn.discordapp.com/attachments/630085034748936202/634193920221380626/unknown.png)
(The font in some of them looks cooler because I changed it to Product Sans since my other screenshots; always learning!)
